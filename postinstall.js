'use strict';

const ncpCallback = require('ncp').ncp;
const rimrafCallback = require('rimraf');
const mkdirpCallback = require('mkdirp');
const path = require('path');
const chalk = require('chalk');
const { spawnSync } = require('child_process');

const ncp = (src, dest) => new Promise((resolve, reject) => ncpCallback(src, dest, (err) => err ? reject(err) : resolve()));
const rimraf = (path) => new Promise((resolve, reject) => rimrafCallback(path, (err) => err ? reject(err) : resolve()));
const mkdirp = (path) => new Promise((resolve, reject) => mkdirpCallback(path, (err) => err ? reject(err) : resolve()));

(async () => {
  await rimraf(path.join(__dirname, 'tmp'));
  await rimraf(path.join(__dirname, 'vendor'));
  await mkdirp(path.join(__dirname, 'tmp'));
  spawnSync('git', [
    'clone',
    'https://github.com/AztecProtocol/AZTEC',
    path.join(__dirname, 'tmp', 'AZTEC')
  ], {
    stdio: 'inherit'
  });
  const current = process.cwd();
  process.chdir(path.join(__dirname, 'tmp', 'AZTEC'));
  spawnSync('git', [
    'checkout',
    'feat-huff-truffle-integration-ho-ho-ho'
  ], {
    stdio: 'inherit'
  });
  spawnSync('git', [
    'fetch',
  ], {
    stdio: 'inherit'
  });
  process.chdir(current);
  await ncp(path.join(__dirname, 'tmp', 'AZTEC', 'packages', 'huff'), path.join(__dirname, 'vendor'));
  await rimraf(path.join(__dirname, 'tmp'));
  await rimraf(path.join(__dirname, 'vendor', 'package.json'));
})().catch((err) => console.error(err.stack || err.message || err));
